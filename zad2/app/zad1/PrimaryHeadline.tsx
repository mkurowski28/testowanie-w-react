"use client";

import React, { useContext } from "react";
import ColorContext from "./ColorContext";

export const PrimaryHeadline = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const { color } = useContext(ColorContext);
  return <h1 style={{ color: color }}>{children}</h1>;
};

export default PrimaryHeadline;
