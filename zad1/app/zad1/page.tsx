"use client";

import Paragraph from "./Paragraph";
import PrimaryHeadline from "./PrimaryHeadline";
import SecondaryHeadline from "./SecondaryHeadline";
import ColorContext from "./ColorContext";
import ColorPicker from "./ColorPicker";
import { useState } from "react";
export default function Zad1() {
  const [color, setColor] = useState<string>("#ffffff");
  return (
    <>
      <ColorContext.Provider value={{ color, setColor }}>
        <ColorPicker />
        <PrimaryHeadline>h1</PrimaryHeadline>
        <Paragraph>para</Paragraph>
        <SecondaryHeadline>h2</SecondaryHeadline>
      </ColorContext.Provider>
    </>
  );
}
