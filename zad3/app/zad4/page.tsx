"use client";
import React, { useEffect, useState } from "react";
import Items from "./items";
import Form from "./Form";
import { ItemInferface } from "./items";
import axios from "axios";
enum ActiveComponent {
  Form = 'Form',
  Items = 'Items'
}

export default function Page4() {
  const [chosen, setfirst] = useState<ActiveComponent>(ActiveComponent.Form)
  const [items, setItems] = useState<ItemInferface[]>([]);
  const url = "https://fakestoreapi.com/products/";

  const addItem = async (data: ItemInferface) => {
    try {
      const response = await axios.post(url, data);
      if (response.status ===200) setItems([...items, response.data]);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  }
  
  useEffect(() => {
    const getData = async () => {
      try {
        const response = await axios.get(url);
        setItems(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    getData();
  }, []);


  return (
    <>
    <div className="flex justify-center gap-5 p-5">

    <button className="text-black bg-white p-3 rounded-2xl" onClick={()=>setfirst(ActiveComponent.Form)}>Form</button>
    <button className="text-black bg-white p-3 rounded-2xl" onClick={()=>setfirst(ActiveComponent.Items)}>Items</button>
    </div>
    {chosen === 'Form' ? <Form handlePost={addItem}/> : <Items  items={items} />}

    </>
  );
}
