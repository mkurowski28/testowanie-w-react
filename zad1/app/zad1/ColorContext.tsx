"use client";

import { createContext } from "react";
type TcolorContext = { color: string; setColor: (color: string) => void };
const ColorContext = createContext<TcolorContext>({
  color: "#ffffff",
  setColor: (color: string) => {},
});
export default ColorContext;
