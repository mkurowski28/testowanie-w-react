"use client";
import React, { useContext } from "react";
import ColorContext from "./ColorContext";

type ParagraphProps = {
  children: React.ReactNode | string;
};

const Paragraph = ({ children }: ParagraphProps) => {
  const { color } = useContext(ColorContext);
  return (
    <>
      <p style={{ color: color }}>{children}</p>
    </>
  );
};

export default Paragraph;
