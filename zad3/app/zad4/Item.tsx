import React from "react";
import PropTypes from "prop-types";
Item.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  price: PropTypes.number,
  description: PropTypes.string,
  image: PropTypes.string,
  optionalObject: PropTypes.object,
  optionalString: PropTypes.string,
  optionalSymbol: PropTypes.symbol,
  optionalElement: PropTypes.element,
  optionalNode: PropTypes.node,
};
function Item({ id, title, price, description, image }) {
  return (
    <>
      <tr>
        <td>{id}</td>
        <td>{title}</td>
        <td>{price}</td>
        <td>{description}</td>
        <td>
          <img src={image} alt={title} />
        </td>
      </tr>
    </>
  );
}

export default Item;
