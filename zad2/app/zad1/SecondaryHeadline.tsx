"use client";

import React, { useContext } from "react";
import ColorContext from "./ColorContext";

const SecondaryHeadline = ({ children }: { children: React.ReactNode }) => {
  const { color } = useContext(ColorContext);
  return <h2 style={{ color: color }}>{children}</h2>;
};

export default SecondaryHeadline;
