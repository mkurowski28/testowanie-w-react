"use client";
import React, { useContext, useState } from "react";
import ColorContext from "./ColorContext";

export default function ColorPicker() {
  const { color, setColor } = useContext(ColorContext);
  const updateColor = (e: React.ChangeEvent<HTMLInputElement>) => {
    setColor(e.target.value);
  };
  return (
    <>
      <div style={{ color: color }} className="colorPicker">
        <label htmlFor="color">Pick a color</label>
        <input
          type="color"
          onChange={updateColor}
          name="color"
          id="color"
          value={color}
        />
        {color}
      </div>
    </>
  );
}
