"use client";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Image from "next/image";
import Item from "./Item";
interface RatingInterface {
  rate: number;
  count: number;
}

export interface ItemInferface {
  id: number;
  title: string;
  price: number;
  description: string;
  image: string;
  category: string;
  rating: RatingInterface;
}

function Items({ items }: { items: ItemInferface[] }) {
  const createRows = (items: ItemInferface[]) =>
    items.map((el) => (
      <Item
        id={el.id}
        description={el.description}
        image={el.image}
        key={el.title}
        price={el.price}
        title={el.title}
      />
    ));
  const table = (
    <>
      <table className="table-auto">
        <tr>
          <th>id</th>
          <th>title</th>
          <th>price</th>
          <th>category</th>
          <th>description</th>
          <th>image</th>
        </tr>
        {createRows(items)}
      </table>
    </>
  );
  return <>{items.length > 0 ? table : <p>loading...</p>}</>;
}

export default Items;
