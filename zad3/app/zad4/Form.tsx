"use client";
import React, { useEffect, useState } from "react";
import { ItemInferface } from "./items";

export default function Form({handlePost}: {handlePost: (data: any) => void}) {
  const addProduct = (e:React.FormEvent<HTMLFormElement>) => {
    const title  = e.target.title.value
    const price  = e.target.price.value
    const description  = e.target.description.value
    const image  = e.target.image.value
    const category  = e.target.category.value
    const obj = {id:null,title,price,description ,image ,category,rating:0}
    handlePost(obj)
  }
  return                        (
    <>
    <div className="form flex flex-col gap-5 items-center justify ">
      <form onSubmit={(e)=>{
        e.preventDefault()
        addProduct(e)
      }}>
      <div className="title-box">
        <label htmlFor="title">Title</label>
        <input className="text-black" type="text" name="title" id="title" />
      </div>
      <div className="price-box">
        <label htmlFor="price">Price</label>
        <input className="text-black" type="number" name="price" id="price" />
      </div>
      <div className="description-box">
        <label htmlFor="description">Description</label>
        <input className="text-black" type="text" name="description" id="description" />
      </div>
      <div className="image-box">
        <label htmlFor="image">Image</label>
        <input className="text-black" type="text" name="image" id="image" />
      </div>
      <div className="category-box">
        <label htmlFor="category">Category</label>
        <input className="text-black" type="text" name="category" id="category" />
      </div>
      <button type="submit" className="bg-white text-black ">Add</button>
      </form>
    </div>
    </>
  )
}